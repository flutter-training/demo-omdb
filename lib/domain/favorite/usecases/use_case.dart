import 'package:demo_movie/common/models/use_case.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/domain/favorite/repositories/favorite_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class GetFavoriteMoviesUseCase
    implements UseCase<List<FavoriteMovie>, NoPayload> {
  final FavoriteRepository repo;

  GetFavoriteMoviesUseCase({this.repo});

  @override
  Future<List<FavoriteMovie>> call(NoPayload _) {
    return repo.getFavoriteMovies();
  }
}

@lazySingleton
@injectable
class GetRecommendedMovieUsecase implements UseCase<FavoriteEntity, NoPayload> {
  final FavoriteRepository repo;

  GetRecommendedMovieUsecase({this.repo});
  @override
  Future<FavoriteEntity> call(NoPayload _) {
    return repo.getRecommendedMovie();
  }
}

@lazySingleton
@injectable
class GetFavoriteMovieByIdUseCase implements UseCase<FavoriteEntity, String> {
  final FavoriteRepository repo;

  GetFavoriteMovieByIdUseCase({this.repo});

  @override
  Future<FavoriteEntity> call(String payload) {
    return repo.getFavoriteMovieById(id: payload);
  }
}

@lazySingleton
@injectable
class SaveFavoriteMovieUseCase
    implements UseCase<FavoriteEntity, FavoriteEntity> {
  final FavoriteRepository repo;

  SaveFavoriteMovieUseCase({this.repo});

  @override
  Future<FavoriteEntity> call(FavoriteEntity payload) {
    return repo.saveFavoriteMovie(movie: payload);
  }
}

@lazySingleton
@injectable
class UpdateFavoriteMovieUsecase implements UseCase<FavoriteEntity, Map> {
  final FavoriteRepository repo;

  UpdateFavoriteMovieUsecase({this.repo});

  @override
  Future<FavoriteEntity> call(Map payload) {
    return repo.updateFavoriteMovie(
      id: payload['id'] as String,
      movie: payload['movie'],
    );
  }
}

@lazySingleton
@injectable
class DeleteFavoriteMovie implements UseCase<bool, String> {
  final FavoriteRepository repo;

  DeleteFavoriteMovie({this.repo});

  @override
  Future<bool> call(String payload) {
    return repo.deleteFavoriteMovie(id: payload);
  }
}
