import 'dart:core';

import 'package:demo_movie/data/favorite_movie/repositories/favorite_repository_impl.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@Bind.toType(FavoriteRepositoryImpl)
@injectable
abstract class FavoriteRepository {
  Future<List<FavoriteEntity>> getFavoriteMovies();
  Future<FavoriteEntity> getRecommendedMovie();
  Future<FavoriteEntity> getFavoriteMovieById({@required String id});
  Future<FavoriteEntity> saveFavoriteMovie({@required FavoriteEntity movie});
  Future<FavoriteEntity> updateFavoriteMovie({
    @required String id,
    @required FavoriteEntity movie,
  });
  Future<bool> deleteFavoriteMovie({@required String id});
}
