import 'dart:convert';

import 'package:flutter/material.dart';

class FavoriteDetailsEntity {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;
  final int priority;
  final int rating;
  final bool viewed;
  final int timestamp;

  FavoriteDetailsEntity({
    @required this.id,
    @required this.title,
    @required this.year,
    @required this.poster,
    @required this.label,
    this.priority,
    this.rating,
    this.viewed,
    this.timestamp,
  });

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'year': year,
        'poster': poster,
        'label': label,
        'priority': priority,
        'rating': rating,
        'viewed': viewed,
        'timestamp': timestamp,
      };

  FavoriteDetailsEntity update({
    final String id,
    final String title,
    final String label,
    final String poster,
    final int priority,
    final int rating,
    final bool viewed,
    final String year,
    final int timestamp,
  }) =>
      FavoriteDetailsEntity(
        id: id ?? this.id,
        title: title ?? this.id,
        label: label ?? this.label,
        poster: poster ?? this.poster,
        priority: priority ?? this.priority,
        rating: rating ?? this.rating,
        viewed: viewed ?? this.viewed,
        year: year ?? this.year,
        timestamp: timestamp ?? this.timestamp,
      );

  @override
  String toString() {
    // TODO: implement toString
    return super.toString();
    // return jsonEncode(this.toJson());
  }
}
