import 'package:flutter/material.dart';

class FavoriteEntity {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;
  final int priority;
  final int rating;
  final bool viewed;
  final int timestamp;

  FavoriteEntity({
    @required this.id,
    @required this.title,
    @required this.year,
    @required this.poster,
    @required this.label,
    this.priority,
    this.rating,
    this.viewed,
    this.timestamp,
  });

  FavoriteEntity update({
    final String id,
    final String title,
    final String year,
    final String poster,
    final String label,
    final int priority,
    final int rating,
    final bool viewed,
    final int timestamp,
  }) =>
      FavoriteEntity(
        id: id ?? this.id,
        title: title ?? this.title,
        year: year ?? this.year,
        poster: poster ?? this.poster,
        label: label ?? this.label,
        priority: priority ?? this.priority,
        rating: rating ?? this.rating,
        viewed: viewed ?? this.viewed,
        timestamp: timestamp ?? this.timestamp,
      );
}
