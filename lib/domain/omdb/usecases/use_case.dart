import 'package:demo_movie/common/models/use_case.dart';
import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_movie/domain/omdb/repositories/omdb_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class GetMovieByIdUsecase implements UseCase<OmdbMovieEntity, String> {
  final OmdbRepository repo;

  GetMovieByIdUsecase({@required this.repo});

  @override
  Future<OmdbMovieEntity> call(String id) {
    return repo.getMoviesById(id: id);
  }
}

@lazySingleton
@injectable
class GetMovieByTitleAndYearUsecase
    implements UseCase<List<OmdbMovieEntity>, Map> {
  final OmdbRepository repo;

  GetMovieByTitleAndYearUsecase({@required this.repo});

  @override
  Future<List<OmdbMovieEntity>> call(Map payload) {
    return repo.getMoviesByTitleAndYear(
      title: (payload['title'] ?? '').toString(),
      year: (payload['year'] ?? '').toString(),
    );
  }
}
