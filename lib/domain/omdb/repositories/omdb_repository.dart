import 'package:demo_movie/data/omdb_movie/repositories/omdb_repository_impl.dart';
import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';
import 'package:injectable/injectable.dart';

@Bind.toType(OmdbRepositoryImpl)
@injectable
abstract class OmdbRepository {
  Future<List<OmdbMovieEntity>> getMoviesByTitleAndYear({
    String title,
    String year,
  }) {}
  Future<OmdbMovieEntity> getMoviesById({
    String id,
  }) {}
}
