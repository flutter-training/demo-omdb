import 'package:flutter/material.dart';

class OmdbMovieEntity {
  final String title;
  final String year;
  final String id;
  final String poster;

  OmdbMovieEntity({
    @required this.title,
    @required this.year,
    @required this.id,
    this.poster,
  });

  factory OmdbMovieEntity.fromJson(Map<String, dynamic> json) =>
      OmdbMovieEntity(
        title: json['Title'],
        year: json['Year'],
        id: json['imdbID'],
        poster: json['Poster'] ?? null,
      );
}

extension MovieExt on OmdbMovieEntity {
  OmdbMovieEntity copyWith(
          {String title, String year, String id, String poster}) =>
      OmdbMovieEntity(
        title: title ?? this.title,
        year: year ?? this.year,
        id: id ?? this.id,
        poster: poster ?? this.poster,
      );
}
