import 'package:demo_movie/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_movie/data/omdb_movie/models/omdb_movie.dart';
import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_movie/domain/omdb/repositories/omdb_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class OmdbRepositoryImpl implements OmdbRepository {
  final OmdbRemoteDatasource omdbRDS;

  OmdbRepositoryImpl({this.omdbRDS});

  @override
  Future<OmdbMovieEntity> getMoviesById({String id}) async {
    return omdbRDS.getOmdbMovieById(id);
  }

  @override
  Future<List<OmdbMovieEntity>> getMoviesByTitleAndYear(
      {String title, String year}) {
    return omdbRDS.getOmdbMovieByTitleAndYear(title, year);
  }
}
