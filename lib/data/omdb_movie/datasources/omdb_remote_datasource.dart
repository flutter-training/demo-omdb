import 'dart:convert';

import 'package:demo_movie/common/network/network_utils.dart';
import 'package:demo_movie/common/network/ws_client.dart';
import 'package:demo_movie/data/omdb_movie/models/omdb_movie.dart';

import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

@Bind.toType(OmdbRemoteDatasourceImpl)
@injectable
abstract class OmdbRemoteDatasource {
  Future<OmdbMovie> getOmdbMovieById(String id) async {}
  Future<List<OmdbMovie>> getOmdbMovieByTitleAndYear(
      String title, String year) async {}
}

@lazySingleton
@injectable
class OmdbRemoteDatasourceImpl implements OmdbRemoteDatasource {
  final MovieClient client;
  final String _omdbHost = 'www.omdbapi.com';
  final String _apiKey = 'bca15354';

  OmdbRemoteDatasourceImpl({this.client});

  factory OmdbRemoteDatasourceImpl.create() {
    return OmdbRemoteDatasourceImpl(
      client: MovieClientImpl(defaultHttp),
    );
  }

  @override
  Future<List<OmdbMovie>> getOmdbMovieByTitleAndYear(
    String title,
    String year,
  ) async {
    http.Response response = await client.get(Uri.https(_omdbHost, '/', {
      'apikey': _apiKey,
      's': title ?? '',
      'y': year ?? '',
      'type': 'movie',
    }));
    Map<String, dynamic> jsonBody = jsonDecode(response.body);
    if (response.statusCode == 200 && !jsonBody.containsKey('Error'))
      return OmdbMovieList.fromJson(jsonBody).search;
    return [];
  }

  @override
  Future<OmdbMovie> getOmdbMovieById(String id) async {
    http.Response response = await client
        .get(Uri.https(_omdbHost, '/', {'apikey': _apiKey, 'i': id}));
    Map<String, dynamic> jsonBody = jsonDecode(response.body);
    if (response.statusCode == 200 && !jsonBody.containsKey('Error'))
      return OmdbMovie.fromJson(jsonBody);
    return null;
  }
}
