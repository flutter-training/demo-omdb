import 'dart:convert';

import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';

class OmdbMovie extends OmdbMovieEntity {
  final String type;

  OmdbMovie(
      {String title, String imdbId, String year, this.type, String poster})
      : super(title: title, id: imdbId, year: year, poster: poster);

  factory OmdbMovie.fromJson(Map<String, dynamic> json) => OmdbMovie(
        title: json['Title'],
        imdbId: json['imdbID'],
        year: json['Year'],
        type: json['Type'],
        poster: json['Poster'],
      );
}

class OmdbMovieList {
  final List<OmdbMovie> search;
  final int totalResults;
  final String response;

  OmdbMovieList({this.search, this.totalResults, this.response});

  factory OmdbMovieList.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    List rawOmdbMovies = json['Search'] ?? [];
    List<OmdbMovie> omdbMovies = [];
    rawOmdbMovies.forEach(
      (raw) => omdbMovies.add(
        OmdbMovie.fromJson(raw),
      ),
    );
    return OmdbMovieList(
      search: omdbMovies,
      totalResults: int.parse(json['totalResults']),
      response: json['Response'],
    );
  }
}
