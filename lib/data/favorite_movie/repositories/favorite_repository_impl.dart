import 'package:demo_movie/data/favorite_movie/datasources/favorite_remote_datasource.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/domain/favorite/repositories/favorite_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class FavoriteRepositoryImpl implements FavoriteRepository {
  final FavoriteMovieRemoteDatasource favoriteRDS;

  FavoriteRepositoryImpl({this.favoriteRDS});

  @override
  Future<bool> deleteFavoriteMovie({String id}) {
    // TODO: implement deleteFavoriteMovie
    return null;
  }

  @override
  Future<FavoriteEntity> getFavoriteMovieById({String id}) {
    // TODO: implement getFavoriteMovieById
    return null;
  }

  @override
  Future<List<FavoriteEntity>> getFavoriteMovies() {
    // TODO: implement getFavoriteMovies
    return null;
  }

  @override
  Future<FavoriteEntity> saveFavoriteMovie({FavoriteEntity movie}) {
    // TODO: implement saveFavoriteMovie
    return null;
  }

  @override
  Future<FavoriteEntity> updateFavoriteMovie(
      {String id, FavoriteEntity movie}) async {
    FavoriteMovie oldMovie = FavoriteMovie.fromEntity(movie);
    FavoriteMovie newMovie =
        await favoriteRDS.editFavoriteMovie(id: id, movie: oldMovie);
    return newMovie == null ? null : newMovie.toEntity();
  }

  @override
  Future<FavoriteEntity> getRecommendedMovie() async {
    FavoriteMovie movie = await favoriteRDS.getRecommendedMovie();
    return movie == null ? null : movie.toEntity();
  }
}
