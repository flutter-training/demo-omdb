import 'dart:convert';

import 'package:demo_movie/common/network/http_constant.dart';
import 'package:demo_movie/common/network/network_utils.dart';
import 'package:demo_movie/common/network/ws_client.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

@Bind.toType(FavoriteMovieRemoteDatasourceImpl)
@injectable
abstract class FavoriteMovieRemoteDatasource {
  Future<List<FavoriteMovie>> getFavoriteMovie();
  Future<FavoriteMovie> getRecommendedMovie();
  Future<FavoriteMovie> getFavoriteMovieById({@required String id});
  Future<FavoriteMovie> addFavoriteMovie({@required FavoriteMovie movie});
  Future<FavoriteMovie> editFavoriteMovie({
    @required String id,
    @required FavoriteMovie movie,
  });
  Future<bool> deleteFavoriteMovieById({@required String id});
}

@lazySingleton
@injectable
class FavoriteMovieRemoteDatasourceImpl
    implements FavoriteMovieRemoteDatasource {
  final MovieClient client;
  final String _host = 'demo-video-ws-chfmsoli4q-ew.a.run.app';
  final String _token = 'tatas1';

  FavoriteMovieRemoteDatasourceImpl({this.client});

  factory FavoriteMovieRemoteDatasourceImpl.create() {
    return FavoriteMovieRemoteDatasourceImpl(
      client: MovieClientImpl(defaultHttp),
    );
  }

  @override
  Future<FavoriteMovie> addFavoriteMovie(
      {@required FavoriteMovie movie}) async {
    Uri uri = Uri.https(_host, '/video-ws/videos/', {});
    Map<String, String> header = {
      'token': _token,
      AppHeaders.CONTENT_TYPE_KEY: AppHeaders.FORM_ENCODED_HEADER,
    };
    http.Response response = await client.post(
      uri,
      headers: header,
      body: jsonEncode(movie.toJson()),
    );
    if (response.statusCode >= 200 && response.statusCode < 300) return movie;
    return null;
  }

  @override
  Future<FavoriteMovie> editFavoriteMovie({
    @required String id,
    @required FavoriteMovie movie,
  }) async {
    Uri uri = Uri.https(_host, '/video-ws/videos/$id', {});
    Map<String, String> header = {
      'token': _token,
      AppHeaders.CONTENT_TYPE_KEY: AppHeaders.FORM_ENCODED_HEADER,
    };
    http.Response response = await client.put(
      uri,
      headers: header,
      body: jsonEncode(movie.toJson()),
    );
    if (response.statusCode >= 200 && response.statusCode < 300) return movie;
    return null;
  }

  @override
  Future<bool> deleteFavoriteMovieById({String id}) async {
    Uri uri = Uri.https(_host, '/video-ws/videos/$id', {});
    Map<String, String> header = {
      'token': _token,
      AppHeaders.CONTENT_TYPE_KEY: AppHeaders.FORM_ENCODED_HEADER,
    };
    http.Response response = await client.delete(
      uri,
      headers: header,
    );
    if (response.statusCode >= 200 && response.statusCode < 300) return true;
    return false;
  }

  @override
  Future<FavoriteMovie> getFavoriteMovieById({String id}) async {
    Uri uri = Uri.https(_host, '/video-ws/videos/$id', {});
    Map<String, String> header = {
      'token': _token,
      AppHeaders.CONTENT_TYPE_KEY: AppHeaders.FORM_ENCODED_HEADER,
    };
    http.Response response = await client.delete(
      uri,
      headers: header,
    );
    if (response.statusCode >= 200 && response.statusCode < 300) return null;
    return null;
  }

  @override
  Future<List<FavoriteMovie>> getFavoriteMovie() async {
    Uri uri = Uri.https(_host, '/video-ws/videos', {});
    Map<String, String> header = {
      'token': _token,
      AppHeaders.CONTENT_TYPE_KEY: AppHeaders.FORM_ENCODED_HEADER,
    };
    http.Response response = await client.get(
      uri,
      headers: header,
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      List rawData = jsonDecode(response.body);
      return FavoriteMovie.fromListJson(rawData);
    }
    return List<FavoriteMovie>();
  }

  @override
  Future<FavoriteMovie> getRecommendedMovie() async {
    Uri uri = Uri.https(_host, '/video-ws/recommended', {});
    Map<String, String> header = {
      'token': _token,
    };
    http.Response response = await client.get(
      uri,
      headers: header,
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      Map<String, dynamic> rawData = jsonDecode(response.body);
      return rawData.isEmpty ? null : FavoriteMovie.fromJson(rawData);
    }
    return null;
  }
}
