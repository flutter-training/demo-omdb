import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';

class FavoriteMovie extends FavoriteEntity {
  FavoriteMovie({
    String id,
    String title,
    String label,
    String poster,
    int priority,
    int rating,
    String year,
    bool viewed,
    int timestamp,
  }) : super(
          id: id,
          title: title,
          year: year,
          poster: poster,
          label: label,
          priority: priority,
          rating: rating,
          viewed: viewed,
          timestamp: timestamp,
        );

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'title': this.title,
      'year': this.year,
      'label': this.label,
      'poster': this.poster,
      'priority': this.priority,
      'rating': this.rating,
      'viewed': this.viewed,
      'timestamp': this.timestamp,
    };
  }

  factory FavoriteMovie.fromJson(Map<String, dynamic> json) => FavoriteMovie(
        id: json['id'],
        title: json['title'],
        year: json['year'],
        poster: json['poster'],
        label: json['label'],
        priority: json['priority'],
        rating: json['rating'],
        viewed: json['viewed'],
        timestamp: json['timestamp'],
      );

  static List<FavoriteMovie> fromListJson(List json) {
    if (json == null || json.isEmpty) return <FavoriteMovie>[];
    List<FavoriteMovie> temp = [];
    json.forEach((j) {
      temp.add(FavoriteMovie.fromJson(j));
    });
    return temp;
  }

  factory FavoriteMovie.fromEntity(FavoriteEntity movie) {
    return FavoriteMovie(
      id: movie.id,
      title: movie.title,
      year: movie.year,
      poster: movie.poster,
      label: movie.label,
      priority: movie.priority,
      rating: movie.rating,
      viewed: movie.viewed,
      timestamp: movie.timestamp,
    );
  }

  FavoriteEntity toEntity() => FavoriteEntity(
        id: this.id,
        title: this.title,
        year: this.year,
        poster: this.poster,
        label: this.label,
        priority: this.priority,
        rating: this.rating,
        viewed: this.viewed,
        timestamp: this.timestamp,
      );

  FavoriteMovie update({
    final String id,
    final String title,
    final String label,
    final String poster,
    final int priority,
    final int rating,
    final bool viewed,
    final String year,
    final int timestamp,
  }) =>
      FavoriteMovie(
        id: id ?? this.id,
        title: title ?? this.title,
        label: label ?? this.label,
        poster: poster ?? this.poster,
        priority: priority ?? this.priority,
        rating: rating ?? this.rating,
        viewed: viewed ?? this.viewed,
        year: year ?? this.year,
        timestamp: timestamp ?? this.timestamp,
      );
}
