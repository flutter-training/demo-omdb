// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:demo_movie/common/network/ws_client.dart';
import 'package:http/src/client.dart';
import 'package:demo_movie/data/favorite_movie/datasources/favorite_remote_datasource.dart';
import 'package:demo_movie/data/favorite_movie/repositories/favorite_repository_impl.dart';
import 'package:demo_movie/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_movie/data/omdb_movie/repositories/omdb_repository_impl.dart';
import 'package:demo_movie/domain/favorite/repositories/favorite_repository.dart';
import 'package:demo_movie/domain/favorite/usecases/use_case.dart';
import 'package:demo_movie/domain/omdb/repositories/omdb_repository.dart';
import 'package:demo_movie/domain/omdb/usecases/use_case.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<MovieClient>(() => MovieClientImpl(getIt<Client>()))
    ..registerLazySingleton<MovieClientImpl>(
        () => MovieClientImpl(getIt<Client>()))
    ..registerFactory<FavoriteMovieRemoteDatasource>(
        () => FavoriteMovieRemoteDatasourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<FavoriteMovieRemoteDatasourceImpl>(
        () => FavoriteMovieRemoteDatasourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<FavoriteRepositoryImpl>(() => FavoriteRepositoryImpl(
        favoriteRDS: getIt<FavoriteMovieRemoteDatasource>()))
    ..registerFactory<OmdbRemoteDatasource>(
        () => OmdbRemoteDatasourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<OmdbRemoteDatasourceImpl>(
        () => OmdbRemoteDatasourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<OmdbRepositoryImpl>(
        () => OmdbRepositoryImpl(omdbRDS: getIt<OmdbRemoteDatasource>()))
    ..registerFactory<FavoriteRepository>(() => FavoriteRepositoryImpl(
        favoriteRDS: getIt<FavoriteMovieRemoteDatasource>()))
    ..registerLazySingleton<GetFavoriteMoviesUseCase>(
        () => GetFavoriteMoviesUseCase(repo: getIt<FavoriteRepository>()))
    ..registerLazySingleton<GetRecommendedMovieUsecase>(
        () => GetRecommendedMovieUsecase(repo: getIt<FavoriteRepository>()))
    ..registerLazySingleton<GetFavoriteMovieByIdUseCase>(() => GetFavoriteMovieByIdUseCase(repo: getIt<FavoriteRepository>()))
    ..registerLazySingleton<SaveFavoriteMovieUseCase>(() => SaveFavoriteMovieUseCase(repo: getIt<FavoriteRepository>()))
    ..registerLazySingleton<UpdateFavoriteMovieUsecase>(() => UpdateFavoriteMovieUsecase(repo: getIt<FavoriteRepository>()))
    ..registerLazySingleton<DeleteFavoriteMovie>(() => DeleteFavoriteMovie(repo: getIt<FavoriteRepository>()))
    ..registerFactory<OmdbRepository>(() => OmdbRepositoryImpl(omdbRDS: getIt<OmdbRemoteDatasource>()))
    ..registerLazySingleton<GetMovieByIdUsecase>(() => GetMovieByIdUsecase(repo: getIt<OmdbRepository>()))
    ..registerLazySingleton<GetMovieByTitleAndYearUsecase>(() => GetMovieByTitleAndYearUsecase(repo: getIt<OmdbRepository>()));
}
