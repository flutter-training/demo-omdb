import 'package:demo_movie/common/routes/routes.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/presentation/dashboard/dashboard_page.dart';
import 'package:demo_movie/presentation/favorite/favorite_details_page.dart';
import 'package:demo_movie/presentation/home/home_page.dart';
import 'package:demo_movie/presentation/login/login_page.dart';
import 'package:demo_movie/presentation/search/search_page.dart';
import 'package:demo_movie/presentation/splash/splash_page.dart';
import 'package:flutter/material.dart';

abstract class Router {
  static Map<String, WidgetBuilder> routes = {
    // Routes.splash: (BuildContext context) => SplashPage(),
    // Routes.login: (BuildContext context) => LoginPage(),
    // Routes.home: (BuildContext context) => HomePage(),
    // Routes.dashboard: (BuildContext context) => DashboardPage(),
    // Routes.search: (BuildContext context) => SearchPage(),
  };

  static Route<dynamic> onGeneratedRoute(RouteSettings settings) {
    final Object _args = settings.arguments;
    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute(builder: (_) => SplashPage());
      case Routes.login:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case Routes.home:
        return MaterialPageRoute(builder: (_) => HomePage());
      case Routes.dashboard:
        return MaterialPageRoute(builder: (_) => DashboardPage());
      case Routes.search:
        return MaterialPageRoute(builder: (_) => SearchPage());
      case Routes.favoriteDetails:
        return MaterialPageRoute(
            builder: (_) =>
                FavoriteDetailsMain(movie: _args as FavoriteEntity));
      default:
        return onUnknownRoute(settings);
    }
  }

  static Route<dynamic> onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) => Scaffold(
        body: Center(
          child: Text('Error'),
        ),
      ),
    );
  }
}
