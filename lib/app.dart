import 'package:demo_movie/common/routes/router.dart';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String appTitle = 'Movie App';
    return MaterialApp(
      title: appTitle,
      initialRoute: Routes.splash,
      onGenerateRoute: Router.onGeneratedRoute,
      onUnknownRoute: Router.onUnknownRoute,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.deepPurple,
      ),
    );
  }
}
