import 'package:demo_movie/app.dart' as app;
import 'package:demo_movie/common/config/injector.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupInjections();
  // // setupLogger()  /// Not Implement yet
  app.main();
}
