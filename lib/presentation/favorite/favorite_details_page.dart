import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/data/favorite_movie/datasources/favorite_remote_datasource.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/domain/favorite/usecases/use_case.dart';
import 'package:demo_movie/presentation/favorite/widget/favorite_movie_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

void main() => runApp(FavoriteDetailsMain());

class FavoriteDetailsMain extends StatelessWidget {
  FavoriteDetailsMain({Key key, this.movie}) : super(key: key);

  final FavoriteEntity movie;
  final formKey = GlobalKey<_FavoriteDetailsBodyState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorite Movie Details'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              formKey.currentState.saveMovie();
            },
          )
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: FavoriteDetailsBody(
        key: formKey,
        movie: movie,
      ),
    );
  }
}

class FavoriteDetailsBody extends StatefulWidget {
  final FavoriteEntity movie;

  const FavoriteDetailsBody({Key key, @required this.movie}) : super(key: key);

  @override
  _FavoriteDetailsBodyState createState() => _FavoriteDetailsBodyState(movie);
}

class _FavoriteDetailsBodyState extends State<FavoriteDetailsBody> {
  _FavoriteDetailsBodyState(this._movie);

  FavoriteEntity _movie;

  FavoriteMovieRemoteDatasource fmRDS = getIt<FavoriteMovieRemoteDatasource>();
  UpdateFavoriteMovieUsecase updateMovie = getIt<UpdateFavoriteMovieUsecase>();

  final formKey = GlobalKey<FormState>();
  final prKey = GlobalKey<PriorityAndRatingFieldState>();
  final dateKey = GlobalKey<MovieDateFieldState>();

  final titleController = TextEditingController();
  final yearController = TextEditingController();
  final labelController = TextEditingController();

  Widget get image {
    String url = _movie.poster ?? '';
    bool isUrlValid = url.isNotEmpty;
    isUrlValid &= url == 'N/A';
    return isUrlValid
        ? Icon(Icons.movie)
        : Image.network(
            _movie.poster,
            fit: BoxFit.cover,
          );
  }

  Widget titleTextField() {
    return TextFormField(
      initialValue: _movie.title,
      // controller: titleController,
      style: Theme.of(context).textTheme.title,
      validator: (str) {
        _movie = _movie.update(title: str);
        return str.isEmpty ? 'Harus diisi' : null;
      },
      // onFieldSubmitted: (value) => _movie = _movie.update(title: value),
      minLines: 1,
      maxLines: 3,
      decoration: InputDecoration(
        labelText: 'Title',
        hintText: 'Title...',
      ),
    );
  }

  Widget yearTextField() {
    return TextFormField(
      initialValue: _movie.year,
      // controller: yearController,
      style: Theme.of(context).textTheme.title,
      keyboardType: TextInputType.numberWithOptions(signed: false),
      validator: (str) {
        _movie = _movie.update(year: str);
        return str.isEmpty ? 'Harus diisi' : null;
      },
      // onFieldSubmitted: (value) => _movie = _movie.update(year: value),
      maxLines: 1,
      decoration: const InputDecoration(
        labelText: 'Year',
        hintText: 'Year...',
      ),
    );
  }

  Widget labelTextField() {
    return TextFormField(
      initialValue: _movie.label,
      // controller: labelController,
      style: Theme.of(context).textTheme.title,
      validator: (str) {
        _movie = _movie.update(label: str);
        return str.isEmpty ? 'Harus diisi' : null;
      },
      minLines: 1,
      maxLines: 3,
      decoration: const InputDecoration(
        labelText: 'Label',
        hintText: 'Label...',
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    titleController.text = _movie.title ?? '';
    yearController.text = _movie.year ?? '';
    labelController.text = _movie.label ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: ListView(
        children: <Widget>[
          FavoriteMovieHeader1(
            title: titleTextField(),
            year: yearTextField(),
            poster: image,
            label: labelTextField(),
          ),
          PriorityAndRatingField(
            key: prKey,
            movie: _movie,
          ),
          ViewedFormField(
            initialValue: _movie.viewed,
            validator: (val) {
              _movie = _movie.update(viewed: val);
              return val == null ? 'Harus diisi' : null;
            },
          ),
          MovieDateField(
            key: dateKey,
            movie: _movie,
          ),
          FlatButton.icon(
            icon: Icon(
              Icons.save,
              color: Theme.of(context).primaryColor,
            ),
            label: Text('Save'),
            onPressed: () {
              saveMovie();
            },
          ),
        ],
      ),
    );
  }

  Future<void> saveMovie() async {
    bool isValidated = formKey.currentState.validate();
    _movie = _movie.update(
      priority: prKey.currentState.priority,
      rating: prKey.currentState.rating,
      timestamp: dateKey.currentState.timestamp,
    );
    if (isValidated) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Saving Data...'),
      ));
      FavoriteEntity newMovie =
          await updateMovie({'id': _movie.id, 'movie': _movie});
      String snackBarText;
      if (newMovie != null)
        snackBarText = 'Saving Data Successed';
      else
        snackBarText = 'Try Agian';
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(snackBarText),
      ));
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Please fill all field'),
      ));
    }
    setState(() {});
  }

  @override
  void dispose() {
    titleController.dispose();
    yearController.dispose();
    labelController.dispose();
    super.dispose();
  }
}
