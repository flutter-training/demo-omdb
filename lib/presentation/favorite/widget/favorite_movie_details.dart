import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

class FavoriteMovieHeader1 extends StatelessWidget {
  const FavoriteMovieHeader1({
    Key key,
    @required this.title,
    @required this.year,
    @required this.label,
    @required this.poster,
  }) : super(key: key);

  final TextFormField title;
  final TextFormField year;
  final TextFormField label;
  final Widget poster;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                title,
                year,
                label,
              ],
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.tight,
          child: poster,
        ),
      ],
    );
  }
}

class FavoriteMovieHeader extends StatelessWidget {
  const FavoriteMovieHeader(
    this._movie, {
    Key key,
    this.titleController,
    this.yearController,
    this.labelController,
  }) : super(key: key);

  final FavoriteMovie _movie;

  final TextEditingController titleController;
  final TextEditingController yearController;
  final TextEditingController labelController;

  Widget get image {
    String url = _movie.poster ?? '';
    bool isUrlValid = url.isNotEmpty;
    isUrlValid &= url == 'N/A';
    return isUrlValid
        ? Icon(Icons.movie)
        : Image.network(
            _movie.poster,
            fit: BoxFit.cover,
          );
  }

  Widget titleTextField(
    BuildContext context,
    TextEditingController controller,
  ) {
    return TextFormField(
      initialValue: _movie.title,
      // controller: controller,
      style: Theme.of(context).textTheme.title,
      validator: (str) => str.isEmpty ? 'Harus diisi' : null,
      minLines: 1,
      maxLines: 3,
      decoration: InputDecoration(
        labelText: 'Title',
        hintText: 'Title...',
      ),
    );
  }

  Widget yearTextField(
    BuildContext context,
    TextEditingController controller,
  ) {
    return TextFormField(
      initialValue: _movie.year,
      // controller: controller,
      style: Theme.of(context).textTheme.title,
      keyboardType: TextInputType.numberWithOptions(signed: false),
      validator: (str) => str.isEmpty ? 'Harus diisi' : null,
      maxLines: 1,
      decoration: const InputDecoration(
        labelText: 'Year',
        hintText: 'Year...',
      ),
    );
  }

  Widget labelTextField(
    BuildContext context,
    TextEditingController controller,
  ) {
    return TextFormField(
      initialValue: _movie.label,
      controller: controller,
      style: Theme.of(context).textTheme.title,
      validator: (str) => str.isEmpty ? 'Harus diisi' : null,
      minLines: 1,
      maxLines: 3,
      decoration: const InputDecoration(
        labelText: 'Label',
        hintText: 'Label...',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                titleTextField(context, titleController),
                yearTextField(context, yearController),
                labelTextField(context, labelController),
              ],
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.tight,
          child: image,
        ),
      ],
    );
  }
}

class PriorityAndRatingField extends StatefulWidget {
  const PriorityAndRatingField({Key key, this.movie}) : super(key: key);

  final FavoriteEntity movie;

  @override
  PriorityAndRatingFieldState createState() =>
      PriorityAndRatingFieldState(movie);
}

class PriorityAndRatingFieldState extends State<PriorityAndRatingField> {
  PriorityAndRatingFieldState(this._movie);

  FavoriteEntity _movie;

  int get priority => _movie.priority ?? 0;
  int get rating => _movie.rating ?? 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                'Priority',
                style: Theme.of(context).textTheme.subtitle,
              ),
              RatingBar(
                direction: Axis.horizontal,
                itemSize: 35,
                itemPadding: EdgeInsets.symmetric(horizontal: 0),
                initialRating: (_movie.priority ?? 0).toDouble(),
                itemCount: 5,
                allowHalfRating: false,
                itemBuilder: (context, _) => Icon(
                  Icons.favorite,
                  color: Colors.red,
                ),
                onRatingUpdate: (value) {
                  _movie = _movie.update(priority: value.toInt());
                },
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Text(
                'Rating',
                style: Theme.of(context).textTheme.subtitle,
              ),
              RatingBar(
                allowHalfRating: false,
                direction: Axis.horizontal,
                initialRating: (_movie.rating ?? 0).toDouble(),
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 0),
                itemSize: 35,
                itemBuilder: (context, _) => Icon(
                  Icons.grade,
                  color: Colors.yellow,
                ),
                onRatingUpdate: (value) {
                  _movie = _movie.update(rating: value.toInt());
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ViewedFormField extends FormField<bool> {
  ViewedFormField({
    BuildContext context,
    FormFieldSetter<bool> onSaved,
    FormFieldValidator<bool> validator,
    bool initialValue,
    bool autovalidate = false,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidate: autovalidate,
          builder: (FormFieldState<bool> state) {
            return Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Viewed',
                    ),
                    FlatButton.icon(
                      onPressed: () {},
                      label: Text('No'),
                      icon: Radio(
                        value: false,
                        groupValue: state.value,
                        onChanged: (value) {
                          state.didChange(value);
                        },
                      ),
                    ),
                    FlatButton.icon(
                      onPressed: () {},
                      label: Text('Yes'),
                      icon: Radio(
                        value: true,
                        groupValue: state.value,
                        onChanged: (value) {
                          state.didChange(value);
                        },
                      ),
                    ),
                    Spacer(),
                  ],
                ),
                state.hasError
                    ? Text(
                        state.errorText,
                        style: TextStyle(color: Colors.red),
                      )
                    : Container(),
              ],
            );
          },
        );
}

class MovieDateField extends StatefulWidget {
  const MovieDateField({Key key, this.movie}) : super(key: key);

  final FavoriteEntity movie;

  @override
  MovieDateFieldState createState() => MovieDateFieldState(movie);
}

class MovieDateFieldState extends State<MovieDateField> {
  MovieDateFieldState(this._movie);

  FavoriteEntity _movie;

  int get timestamp => _movie.timestamp ?? 0;

  DateTime _getDate(int epoch) => DateTime.fromMillisecondsSinceEpoch(epoch);
  String _formatDate(DateTime dt) => DateFormat.yMMMd().format(dt);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: FlatButton.icon(
        icon: Icon(
          Icons.calendar_today,
          color: Theme.of(context).primaryColor,
        ),
        label: Text(
          (_movie.timestamp ?? 0) == 0
              ? 'Pick a date'
              : _formatDate(_getDate(_movie.timestamp)),
        ),
        onPressed: () async {
          DateTime newDate = await showDatePicker(
              context: context,
              initialDate: (_movie.timestamp ?? 0) == 0
                  ? DateTime.now()
                  : _getDate(_movie.timestamp),
              firstDate: DateTime(0),
              lastDate: DateTime.now());
          int epoch = newDate == null ? 0 : newDate.millisecondsSinceEpoch;
          print(epoch);
          setState(() {
            _movie = _movie.update(timestamp: epoch);
          });
        },
      ),
    );
  }
}
