import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:demo_movie/data/favorite_movie/datasources/favorite_remote_datasource.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:flutter/material.dart';

class FavoriteMoviePage extends StatefulWidget {
  const FavoriteMoviePage({Key key}) : super(key: key);
  @override
  _FavoriteMoviePageState createState() => _FavoriteMoviePageState();
}

class _FavoriteMoviePageState extends State<FavoriteMoviePage> {
  List<FavoriteMovie> _movies = [];
  final FavoriteMovieRemoteDatasource fmRDS =
      getIt<FavoriteMovieRemoteDatasource>();

  @override
  void initState() {
    super.initState();
  }

  Widget _buildGridView() {
    return GridView.builder(
      padding: EdgeInsets.all(16),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 180.0,
        childAspectRatio: 0.5,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      itemCount: _movies.length,
      itemBuilder: _itemBuilder,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: FutureBuilder(
          initialData: _movies,
          future: fmRDS.getFavoriteMovie(),
          builder: (context, snapshot) {
            // _getData();
            if (snapshot.connectionState == ConnectionState.none) {
              return AlertDialog(title: Text('Network Error'));
            } else if (snapshot.hasData) {
              _movies = snapshot.data as List<FavoriteMovie>;
              return _buildGridView();
            } else if (snapshot.error != null) {
              return ListTile(title: Text('Error while getting favorite list'));
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    FavoriteMovie _movie = _movies[index];
    return Card(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: (_movies[index].poster ?? 'N/A') == 'N/A'
                ? Icon(
                    Icons.image,
                    size: 150,
                    color: Colors.grey,
                  )
                : Image.network(
                    _movie.poster,
                    fit: BoxFit.cover,
                  ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  Text(
                    _movie.title,
                    maxLines: 2,
                    style: Theme.of(context).textTheme.title,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        _movie.year,
                        maxLines: 1,
                        style: Theme.of(context).textTheme.subtitle,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Row(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                Routes.favoriteDetails,
                                arguments: _movie,
                              );
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () async {
                              // TODO: Implement delete movie by id
                              bool isDeleted = await fmRDS
                                  .deleteFavoriteMovieById(id: _movie.id);
                              if (!isDeleted) {
                                showDialog(
                                  context: context,
                                  child: AlertDialog(
                                    title: Text('Alert!'),
                                    content: Text(
                                        'Delete movie failed, you dumb ass!'),
                                    actions: <Widget>[
                                      FlatButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        child: Text(
                                          'Dismiss',
                                          style: TextStyle(color: Colors.red),
                                        ),
                                      ),
                                    ],
                                    elevation: 10,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                );
                              } else {
                                Scaffold.of(context).showSnackBar(
                                    SnackBar(content: Text('Movie deleted')));
                                setState(() {});
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
