import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/common/routes/router.dart';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: Router.onGeneratedRoute,
      onUnknownRoute: Router.onUnknownRoute,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Movie App'),
        ),
        body: Container(
          padding: EdgeInsets.all(25),
          child: _LoginForm(),
        ),
      ),
    );
  }
}

class _LoginForm extends StatefulWidget {
  const _LoginForm({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<_LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _tokenTextController = TextEditingController();
  final TextEditingController _userTextController = TextEditingController();
  final _prefs = getIt<SharedPreferences>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Image.asset('assets/logo.png'),
          TextFormField(
            controller: _userTextController,
            onChanged: (text) {
              _formKey.currentState.validate();
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Username should be filled';
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: "Username",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
            ),
          ),
          SizedBox(height: 8),
          TextFormField(
            controller: _tokenTextController,
            onChanged: (text) {
              _formKey.currentState.validate();
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Token should be filled';
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: "Your Token Here",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text('Processing Data')));
                  _prefs.setString('username', _userTextController.text);
                  _prefs.setString('token', _tokenTextController.text);
                  Navigator.of(context).pushReplacementNamed(Routes.home);
                }
              },
              child: Text('Submit'),
              color: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
            ),
          ),
        ],
      ),
    );
  }
}
