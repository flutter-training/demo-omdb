import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_movie/domain/omdb/usecases/use_case.dart';
import 'package:demo_movie/presentation/search/widget/movie_list_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  /// State Variable
  String _keyword = '';
  List<OmdbMovieEntity> _movies = [];
  DateTime _selectedYear;

  // final OmdbRemoteDatasource omdbRDS = getIt<OmdbRemoteDatasource>();
  final GetMovieByTitleAndYearUsecase getMoviesByTitleAndYear =
      getIt<GetMovieByTitleAndYearUsecase>();

  final _searchTextController = TextEditingController();

  @override
  void initState() {
    // _getData();
    super.initState();
    _searchTextController.addListener(_onTextChanged);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8),
            child: TextField(
              onChanged: (value) {},
              controller: _searchTextController,
              decoration: InputDecoration(
                labelText: "Search",
                hintText: "Movie Title...",
                suffixIcon: FlatButton(
                  onPressed: () {
                    showDialog(context: context, child: _yearPickerDialog());
                  },
                  child: Text(_selectedYear == null
                      ? 'All'
                      : DateFormat.y().format(_selectedYear)),
                ),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(25.0),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<List<OmdbMovieEntity>>(
              initialData: _movies,
              future: getMoviesByTitleAndYear({
                'title': _keyword,
                'year': _selectedYear,
              }),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.none)
                  return AlertDialog(title: Text('Network Error'));
                if (snapshot.hasData)
                  return MovieListView(listMovie: snapshot.data);
                else if (snapshot.error != null)
                  return ListTile(title: Text('Search Error'));
                else
                  return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }

  Widget _yearPickerDialog() {
    return Dialog(
      child: SizedBox(
        width: 100,
        height: 300,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                'Year :',
                style: Theme.of(context).textTheme.headline,
              ),
            ),
            Expanded(
              flex: 8,
              child: YearPicker(
                selectedDate: _selectedYear ?? DateTime.now(),
                onChanged: (date) {
                  setState(() {
                    _selectedYear = date;
                    Navigator.of(context).pop();
                  });
                },
                firstDate: DateTime(0),
                lastDate: DateTime.now(),
              ),
            ),
            Expanded(
              flex: 1,
              child: FlatButton(
                onPressed: () {
                  setState(() {
                    _selectedYear = null;
                    Navigator.of(context).pop();
                  });
                },
                child: Text('Clear'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onTextChanged() {
    String newText = _searchTextController.text ?? _keyword;
    if (newText == _keyword) return;
    setState(() {
      _keyword = _searchTextController.text;
    });
    // }
  }
}
