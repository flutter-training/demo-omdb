import 'package:demo_movie/common/routes/routes.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/domain/omdb/entities/omdb_entity.dart';
import 'package:flutter/material.dart';

class MovieListView extends StatelessWidget {
  final Widget emptyList;
  const MovieListView({
    Key key,
    @required List<OmdbMovieEntity> listMovie,
    this.emptyList = const ListTile(
      title: const Text("Whoaps! I'm empty"),
    ),
  })  : _listMovie = listMovie,
        super(key: key);

  final List<OmdbMovieEntity> _listMovie;

  @override
  Widget build(BuildContext context) {
    return _listMovie.isEmpty
        ? emptyList
        : ListView.builder(
            padding: EdgeInsets.all(10),
            itemCount: _listMovie.length,
            itemBuilder: (context, index) =>
                MovieListItem(movie: _listMovie[index], context: context),
          );
  }
}

abstract class ListItem {}

class MovieListItem extends StatelessWidget implements ListItem {
  final OmdbMovieEntity movie;
  final BuildContext context;

  const MovieListItem({
    Key key,
    @required this.movie,
    @required this.context,
  }) : super(key: key);

  Widget get poster => DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
        ),
        child: movie.poster == null || movie.poster == 'N/A'
            ? Icon(Icons.movie, size: 300)
            : Image.network(
                movie.poster,
                fit: BoxFit.cover,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: CircularProgressIndicator(
                      value: loadingProgress.expectedTotalBytes != null
                          ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes
                          : null,
                    ),
                  );
                },
              ),
      );

  Widget title(BuildContext context) => ListTile(
        title: Text(
          movie.title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.title,
        ),
        subtitle: Text(
          movie.year,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.subtitle,
        ),
        trailing: IconButton(
          icon: Icon(Icons.favorite_border),
          onPressed: () {
            Navigator.of(context).pushNamed(
              Routes.favoriteDetails,
              arguments: FavoriteEntity(
                id: movie.id,
                title: movie.title,
                year: movie.year,
                poster: movie.poster,
                label: '',
              ),
            );
          },
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        margin: EdgeInsets.all(10),
        elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ListTile(title: poster),
            title(context),
          ],
        ),
      ),
    );
  }
}
