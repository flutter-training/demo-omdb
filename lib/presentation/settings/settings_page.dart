import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text('Setting Page'),
          RecomendedCheckbox(),
          FlatButton.icon(
            onPressed: () async {
              final SharedPreferences _prefs = getIt<SharedPreferences>();
              bool cleared = await _prefs.clear();
              if (cleared)
                Navigator.pushReplacementNamed(context, Routes.login);
            },
            icon: Icon(Icons.exit_to_app),
            label: Text('Logout'),
          ),
        ],
      ),
    );
  }
}

class RecomendedCheckbox extends StatefulWidget {
  @override
  _RecomendedCheckboxState createState() => _RecomendedCheckboxState();
}

class _RecomendedCheckboxState extends State<RecomendedCheckbox> {
  bool _checked;
  final SharedPreferences _prefs = getIt<SharedPreferences>();

  @override
  void initState() {
    _checked = _prefs.containsKey('showRecommended');
    if (_checked) _checked &= _prefs.getBool('showRecommended');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: _checked,
      title: Text('Show recommended'),
      onChanged: (val) {
        _prefs.setBool('showRecommended', val);
        setState(() {
          _checked = val;
        });
      },
    );
  }
}
