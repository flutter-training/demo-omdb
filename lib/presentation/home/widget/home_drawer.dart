import 'package:flutter/material.dart';

typedef HomeCallback = Function(int index);

class HomeDrawer extends StatefulWidget {
  final HomeCallback callback;
  const HomeDrawer({Key key, @required this.callback}) : super(key: key);

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          AppBar(
            title: Text('Movie App'),
            automaticallyImplyLeading: false,
          ),
          FlatButton(
            onPressed: _showDashboardPage,
            child: Text('Dashboard'),
          ),
          FlatButton(
            onPressed: _showSearchPage,
            child: Text('Search'),
          ),
          FlatButton(
            onPressed: _showFavoritePage,
            child: Text('Favorite'),
          ),
          FlatButton(
            onPressed: _showSettingsPage,
            child: Text('Settings'),
          ),
        ],
      ),
    );
  }

  void _showDashboardPage() {
    widget.callback(0);
    Navigator.of(context).pop();
  }

  void _showSearchPage() {
    widget.callback(1);
    Navigator.of(context).pop();
  }

  void _showFavoritePage() {
    widget.callback(2);
    Navigator.of(context).pop();
  }

  void _showSettingsPage() {
    widget.callback(3);
    Navigator.of(context).pop();
  }
}
