import 'package:demo_movie/presentation/dashboard/dashboard_page.dart';
import 'package:demo_movie/presentation/favorite/favorite_page.dart';
import 'package:demo_movie/presentation/home/widget/home_drawer.dart';
import 'package:demo_movie/presentation/search/search_page.dart';
import 'package:demo_movie/presentation/settings/settings_page.dart';
import 'package:flutter/material.dart';

enum HomePageOptions {
  dashboard,
  search,
  favorite,
  settings,
}

class HomePage extends StatefulWidget {
  final HomePageOptions page;
  const HomePage({Key key, this.page}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedPageIndex;

  List<Widget> _pages = [
    DashboardPage(),
    SearchPage(),
    FavoriteMoviePage(),
    SettingsPage(),
  ];

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = widget.page?.index ?? 0;
  }

  void _onItemSelected(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie App'),
      ),
      drawer: HomeDrawer(callback: _onItemSelected),
      body: _pages[_selectedPageIndex],
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  Widget _bottomNavigationBar() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search),
          title: Text('Search'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite_border),
          title: Text('Favorite'),
          activeIcon: Icon(Icons.favorite),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings_applications),
          title: Text('Settings'),
        ),
      ],
      currentIndex: _selectedPageIndex,
      backgroundColor: Theme.of(context).secondaryHeaderColor,
      unselectedItemColor: Colors.grey,
      showUnselectedLabels: false,
      selectedItemColor: Theme.of(context).primaryColor,
      onTap: _onItemSelected,
    );
  }
}
