import 'dart:async';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key key}) : super(key: key);
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool _authenticated;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(child: Image.asset('assets/logo.png')),
    );
  }

  @override
  void initState() {
    super.initState();
    startSplash();
  }

  startSplash() async => Timer(const Duration(seconds: 2), () async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        _authenticated = prefs.containsKey('username') ?? false;
        _authenticated &= prefs.containsKey('token') ?? false;
        Navigator.of(context).pushReplacementNamed(
          _authenticated ? Routes.home : Routes.login,
        );
      });
}
