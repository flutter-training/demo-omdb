import 'package:demo_movie/common/config/injector.dart';
import 'package:demo_movie/common/routes/routes.dart';
import 'package:demo_movie/data/favorite_movie/datasources/favorite_remote_datasource.dart';
import 'package:demo_movie/data/favorite_movie/models/favorite_movie.dart';
import 'package:demo_movie/domain/favorite/entities/favorite_entity.dart';
import 'package:demo_movie/domain/favorite/usecases/use_case.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final FavoriteMovieRemoteDatasource fmRDS =
      FavoriteMovieRemoteDatasourceImpl.create();

  final _prefs = getIt<SharedPreferences>();
  bool _showRecommended;

  @override
  void initState() {
    _showRecommended = _prefs.getBool('showRecommended') ?? false;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _showRecommended = _prefs.getBool('showRecommended') ?? false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _showRecommended
              ? _RecommendedMovie()
              : Container(
                  child: Text('No Recommended Movie'),
                ),
        ],
      ),
    );
  }
}

class _RecommendedMovie extends StatelessWidget {
  _RecommendedMovie({Key key}) : super(key: key);

  final GetRecommendedMovieUsecase _getRecommendedMovie =
      getIt<GetRecommendedMovieUsecase>();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<FavoriteEntity>(
      initialData: null,
      future: _getRecommendedMovie.call(null),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return FavoriteMovieCard(movie: snapshot.data, isRecommended: true);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}

class FavoriteMovieCard extends StatelessWidget {
  const FavoriteMovieCard({
    Key key,
    @required this.movie,
    this.isRecommended = false,
  }) : super(key: key);

  final FavoriteEntity movie;
  final bool isRecommended;

  @override
  Widget build(BuildContext context) {
    final Widget poster = (movie.poster ?? 'N/A') == 'N/A'
        ? Icon(Icons.movie)
        : Image.network(
            movie.poster,
            fit: BoxFit.cover,
          );
    return Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: poster,
          ),
          Expanded(
            flex: 1,
            child: ListTile(
              title: Text(movie.title),
              subtitle: Text(movie.year),
              trailing: isRecommended
                  ? IconButton(
                      icon: Icon(Icons.favorite_border),
                      onPressed: () {
                        Navigator.of(context).pushNamed(Routes.favoriteDetails,
                            arguments: movie);
                      },
                    )
                  : null,
            ),
          ),
        ],
      ),
    );
  }
}
